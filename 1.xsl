﻿<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output indent="yes"/>
	<xsl:output method="html"/>
	<xsl:template match="/">	
		<html>
			<head>
				<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                <style type="text/css">
					h1{ color: #f527bb;font-size: 22px; }
					#headerId, #courseId {font-size: 18px; font-weight: bold; padding-top: 15px; margin-bottom: 15px;}
					#headerId h1, #courseId h1 {color: #f527bb;font-size: 20px; }
					#leftList { float: right;}
					#rightList { float: left;}
					#buisnessPartner, #leaderSchool {font-size: 18px; margin-bottom: 10px; margin-top: 10px; clear: both}
					#buisnessPartner p, #leaderSchool p {font-weight: bold; margin-top: 10px; clear: both}
					#buisnessPartner p, #leaderSchool p {font-weight: bold; margin-top: 10px;}
					#date {font-weight: bold; float: right;}
					#leftList{float: left;};
					#right-list{float: right;};
					
                </style>
			</head>
			<body>
				<h1>Сертификат</h1>
				<div id = "headerId"><xsl:apply-templates select="*/article[@id='headerId']"/></div>
				<div id = "courseId"><xsl:apply-templates select="*/article[@id='courseId']"/></div>
			    <div id = "courseListId"><xsl:apply-templates select="*/article[@id='courseListId']"/></div>
				<div id = "buisnessPartner"><xsl:apply-templates select="*/article[@id='signatureBuisnessPartner']"/></div>
				<div id = "leaderSchool"><xsl:apply-templates select="*/article[@id='signatureLeaderSchool']"/></div>
				<div id = "date"><xsl:apply-templates select="*/article[@id='date']"/></div>
			</body>
		</html>
	</xsl:template>
	
	<xsl:template match="*/article[@id='headerId']">
        удостоверяет, что
		<h1><xsl:value-of select="name-pupil"/></h1>
		c <xsl:value-of select="date-from"/> по <xsl:value-of select="date-to"/>
    </xsl:template>
	
	<xsl:template match="*/article[@id='courseId']">
        прошла обучение в <h1><xsl:value-of select="name-school"/></h1>
        продолжительностью <xsl:value-of select="duration"/> по темам
		<xsl:for-each select="themes">
		     и
			<xsl:value-of select="value"/>
		</xsl:for-each>
    </xsl:template>
	
	<xsl:template match="*/article[@id='courseListId']">
	    <div id = "leftList">
			<ul>
			<xsl:for-each select="left-list/li">
				<li><xsl:value-of select="."/></li>
			</xsl:for-each>
			</ul>
		</div>
		<div id = "rightList">
			<ul>
			<xsl:for-each select="right-list/li">
				<li><xsl:value-of select="."/></li>
			</xsl:for-each>
			</ul>
		</div>
    </xsl:template>
	
	<xsl:template match="*/article[@id='signatureBuisnessPartner']">
        <xsl:value-of select="title"/>
        <p><xsl:value-of select="name-partner"/></p>
    </xsl:template>
	
	<xsl:template match="*/article[@id='signatureLeaderSchool']">
        <xsl:value-of select="title"/>
        <p><xsl:value-of select="name-partner"/></p>
    </xsl:template>
	
	<xsl:template match="*/article[@id='date']">
        Дата: <xsl:value-of select="date-issuance"/>
    </xsl:template>
	
	
</xsl:stylesheet>