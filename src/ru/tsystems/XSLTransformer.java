package ru.tsystems;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by yzharnas on 17.11.2014.
 */
public class XSLTransformer {
    public static void main(String[] args) {
        try {
            File sf = new File(args[0]); // source file
            File rf = new File(args[1]); // result file
            File tf = new File(args[2]); // template file
            if(!rf.exists())
                rf.createNewFile();
            TransformerFactory f = TransformerFactory.newInstance();
            Transformer t = f.newTransformer(new StreamSource(tf));
            t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            Source s = new StreamSource(sf);
            Result r = new StreamResult(new FileOutputStream(rf));
            t.transform(s,r);
        } catch (TransformerConfigurationException e) {
            System.out.println(e.toString());
        } catch (TransformerException e) {
            System.out.println(e.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
